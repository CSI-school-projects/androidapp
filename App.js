import { StatusBar } from 'expo-status-bar';
import React, { useState } from 'react';
import { 
  Button, 
  ImageBackground, 
  StyleSheet, 
  Text, 
  TextInput, 
  View, 
  FlatList,
  Modal,
  Alert,
  Pressable,
} from 'react-native';

export default function App() {
  const SampleGoals = [
    {
      id: '1', 
      title: 'Faire les courses',
    },
    {
      id: '2',
      title: 'Aller à la salle de sport 3 fois par semaine',
    }, 
    {
      id: '3',
      title: 'Monter à plus de 5000m d altitude',
    }, 
    {
      id: '4',
      title: 'Acheter mon premier appartement',
    },
    {
      id: '5',
      title: 'Perdre 5 kgs',
    }, 
    {
      id: '6',
      title: 'Gagner en productivité',
    },
    {
      id: '7',
      title: 'Apprendre un nouveau langage',
    }, 
    {
      id: '8',
      title: 'Faire une mission en freelance',
    },
    {
      id: '9',
      title: 'Organiser un meetup autour de la tech',
    }, 
    {
      id: '10',
      title: 'Faire un triathlon',
    },
  ];

  // State
  const [modal, setModal] = useState(false);
  const [text, setText] = useState('');
  const [list, setList] = useState(SampleGoals);
  // Séparateur
  const Separator = () => ( <View style={styles.separator} /> );
  // Affichage des items
  const renderItem = ({ item }) => (
    <View style={styles.liste} key={item}>
    <Text style={styles.item}>- {item.title} </Text>
    <Modal
        transparent={true}
        visible={modal}
        onRequestClose={() => {
          setModal(!modal);
        }}
    >
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.modalText}>Modal</Text>
          <Button
            style={styles.button}
            title="Fermer"
            onPress={() => setModal(!modal)}
          />
        </View>
      </View>
    </Modal>
    <Button
      style={styles.button}
      title="Modifier"
      onPress={() => setModal(true)}
    />
    <Button
      style={styles.button} 
      title="X"
      onPress={() => onPressDeleteButton(item.id)}
    />
    </View>
  );
  // Affichage de la liste
  const Liste = () => { 
    return <FlatList
      data={SampleGoals}
      renderItem = {renderItem}
      keyExtractor = {item => item.id }
    />
    // return list.map((element, index) => {
    //   return <View style={styles.liste} key={index}>
    //         <Text style={styles.item}>- {element.title} </Text>
    //         <Button 
    //           style={styles.listButton} 
    //           title="X"
    //           onPress={() => onPressDeleteButton(element.id)}
    //         />
    //         </View>
    // })
  }
  // Fonctions 
  const onPressAddButton = () => {
    setList(
      [...list, {id:list.length+1, title:text.toString()}]
    );
  }
  const onPressDeleteButton = (id) => {
    const filtreList = list.filter(item => item.id !== id);
    return setList(filtreList);
  }
  const image = "./assets/background2.jpg";

  return (
    <View style={styles.container}>
      <ImageBackground source={require(image)} resizeMode="center" style={styles.image}>
        <Text style={styles.text}>Listes des <Text style={styles.app}>objectifs</Text> à faire !</Text>
        <StatusBar style="auto" />
        <Liste/>
        <Separator/>
        <TextInput
          style={styles.input}
          value = { text }
          placeholderTextColor='#FFFFFF' 
          placeholder="Rentrez du texte"
          onChangeText={(input) => setText(input)}
        />
        <Separator/>
        <Button
          title="Add"
          underlayColor="white"
          style={styles.addButton}
          onPress={onPressAddButton}
        />
      </ImageBackground>
    </View>
  );
}

/*
 * STYLES
*/

const styles = StyleSheet.create({
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonClose: {
    backgroundColor: "#2196F3",
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    textAlign: 'center',
    marginRight: 30
  },
  addButton: {
    marginHorizontal: 50,
  },
  liste: {
    flexDirection: 'row',
    marginLeft: 10,
  },
  container: {
    flex: 1,
    backgroundColor: '#000',
    justifyContent: 'center',
    alignItems: "center",
    marginTop: 40
  },
  text: {
    color: '#DC143C',
    fontSize: 20,
    padding: 20,
    textAlign: 'center',
  },
  separator: {
    marginVertical: 8,
    marginHorizontal: 15,
    borderBottomColor: '#FFFFFF',
    borderBottomWidth: StyleSheet.hairlineWidth,
    alignSelf:'stretch'
  },
  input: {
    borderColor: '#0066cc',
    color: '#FFFFFF',
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
  app: {
    fontWeight: 'bold',
  },
  item: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#00FF00',
    marginBottom: 10,
  },
  image: {
    flex: 1,
    justifyContent: 'center',
  },
});
